function FilterController($scope){

	angular.element(document).ready(function () {

		$(".select select").change(function(){
			url = $(this).val();
			window.location.replace(url);
		});

		var range = new Array();
		range.min = $("#filter_price").data("slider-min"); // Минимальная граница
		range.max = $("#filter_price").data("slider-max"); // Максимальная граница
		value = $("#filter_price").data("slider-value");
		range.min_value = value[0]; 
		range.max_value = value[1];

	    if($('div').hasClass('filter_price')) {
	        price_range_ini();
	    }

		function price_range_ini() {
		    $(".slider-range").slider({
		        range: true,
		        min: range.min,
		        max: range.max,
		        values: [range.min_value, range.max_value],
		        slide: function( event, ui ) {
		            $('.min_price').val(ui.values[0]);
		            $('.max_price').val(ui.values[1]);
		            set_price_range();
		        }
		    });
		    $('.min_price').val(range.min_value);
		    $('.max_price').val(range.max_value);

		}

		function set_price_range() {
		    var min = parseInt($('.min_price').val());
		    var max = parseInt($('.max_price').val());
		    if(min<range.min||min>range.max||min>max||!min){min = range.min;}
		    if(max>range.max||max<range.min||max<min||!max){max = range.max;}
		    $(".slider-range").slider("values", [min, max]);
		    $('.min_price').val(min);
		    $('.max_price').val(max);

            var q = queryString.parse(location.search);
            q.priceMin = min;
            q.priceMax = max;
            url = '?' + queryString.stringify(q);
            window.location.replace(url);
		}
			
	});

	$scope.resetFilter = function() {

	}

	$scope.pagination = function(obj) {

	}

}