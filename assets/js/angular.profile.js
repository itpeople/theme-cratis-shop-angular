function ProfileController($scope){

	$scope.addressMain = function(obj) {
		$( ".addr_list li" ).each(function( index ) {
			$(this).removeClass('active');
		});
		$(obj.target).parent().parent().addClass('active');
	}

	$scope.addressRemove = function(obj) {
		$(obj.target).parent().parent().remove();
	}

}