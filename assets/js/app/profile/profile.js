

app.config(['$interpolateProvider', '$routeProvider', function ($interpolateProvider, $routeProvider) {

        $routeProvider.
            when('/', {
                templateUrl: '/static/js/app/profile/templates/main.html',
                controller: 'ProfileMainCtrl',
                label: 'Личный кабинет'
            }).
            when('/orders/', {
                templateUrl: '/static/js/app/profile/templates/orders.html',
                controller: 'ProfileOrdersCtrl',
                label: 'Заказы'
            }).
            when('/orders/:type/:id?', {
                templateUrl: '/static/js/app/profile/templates/orders.html',
                controller: 'ProfileOrdersCtrl',
                label: 'orders'
            }).
            when('/info', {
                templateUrl: '/static/js/app/profile/templates/info.html',
//                controller: 'ProfileInfoCtrl',
                label: 'Личная информация'
            }).
            when('/addresses', {
                templateUrl: '/static/js/app/profile/templates/addreses.html',
                controller: 'ProfileAddressCtrl',
                label: 'Адреса доставки'
            }).
            when('/email', {
                templateUrl: '/static/js/app/profile/templates/email.html',
//                controller: 'ProfileInfoCtrl',
                label: 'Изменение email\'a'
            }).
            otherwise({
                redirectTo: '/'
            });

    }]
);


app.directive('breadcrumbs', ['Cart', 'breadcrumbs', function (Cart, breadcrumbs) {
    return {
        restrict: 'E',
        templateUrl: '/static/js/app/profile/templates/breadcrumbs.html',

        scope: {},

        link: function (scope, elm, attrs) {
            scope.breadcrumbs = breadcrumbs;
        }
    };
}]);



app.directive('shopAddress', function () {
    return {
        restrict: 'E',
        templateUrl: '/static/js/app/profile/templates/address.html',

        scope: {
            address: '=data',
            edit: '=edit'
        },

        link: function ($scope, elm, attrs) {
            $scope.countries = CONST_COUNTRIES;
        }
    };
});


app.controller('ProfileCtrl', ['$scope', '$routeParams', '$http', 'djangoRMI', function ($scope, $routeParams, $http, djangoRMI) {

    $scope.orders_loaded = false;

    djangoRMI.cratis_profile__main.get_orders().success(function(data) {
        $scope.all_orders = data;
        $scope.orders_loaded = true;
    });

    $scope.updateAddresses = function(callback) {
        $scope.addresses_loaded = false;
        djangoRMI.cratis_profile__main.get_addresses().success(function(data) {
            $scope.addresses = [];
            angular.forEach(data, function(address) {
                if (address.country) {
                    address.country = CONST_COUNTRIES[address.country];
                } else {
                    address.country = None
                }
                $scope.addresses.push(address);
            });


            $scope.addresses_loaded = true;

            if (callback) {
                callback($scope.addresses);
            }
        });
    };
    $scope.updateAddresses();
}]);


app.controller('ProfileMainCtrl', ['$scope', function ($scope) {

    $scope.openOrder = function(order) {
        location.hash = '#/orders/' + order.state + '/' + order.id;
    };

    $scope.$watch('all_orders', function() {
        if ($scope.all_orders) {
            $scope.orders = $scope.all_orders.all;
        }
    });
}]);


app.controller('ProfileOrdersCtrl', ['$scope', '$routeParams', 'breadcrumbs', function ($scope, $routeParams, breadcrumbs) {

    $scope.$parent.order_type = $routeParams.type;
    $scope.$parent.selected_order = $routeParams.id;

    var type_names = {
        'all': 'Все заказы',
        'paid': 'Оплаченные заказы',
        'unpaid': 'Неоплаченные заказы'
    };
    $scope.page_name = type_names[$routeParams.type];

    breadcrumbs.options = {'orders': $scope.page_name};

    $scope.$watch('all_orders', function() {
        if ($scope.all_orders) {
            if (!$routeParams.type || $routeParams.type == 'all') {
                $scope.orders = $scope.all_orders.all;
            }
            if ($routeParams.type == 'paid') {
                $scope.orders = $scope.all_orders.paid;
            }
            if ($routeParams.type == 'sent') {
                $scope.orders = $scope.all_orders.sent;
            }
            if ($routeParams.type == 'unpaid') {
                $scope.orders = $scope.all_orders.unpaid;
            }
        }
    });

}]);


app.controller('ProfileInfoCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {

}]);



app.controller('ProfileAddressCtrl', ['$scope', '$routeParams', 'djangoRMI', function ($scope, $routeParams, djangoRMI) {
    $scope.form = {
        show_form: false,

        saving: false,

        new_address: {},

        submit: function() {
            $scope.form.saving = true;

            var address = angular.copy($scope.form.new_address);
            address.country = address.country.code;

            djangoRMI.cratis_profile__main.save_address(address).success(function(data) {
                $scope.updateAddresses(function() {
                    $scope.form.saving = false;
                    $scope.form.show_form = false;
                    $scope.form.new_address = {};
                });
            });
        }
    };

    $scope.saveAddress = function(address) {
        var data = angular.copy(address);
        data.country = data.country.code;
        djangoRMI.cratis_profile__main.save_address(data).success(function(data) {
            address.edit = false;
        });
    };

    $scope.deleteAddress = function(address) {
        djangoRMI.cratis_profile__main.delete_address(address).success(function(data) {
            $scope.updateAddresses();
        });
    }


}]);
