
var app = angular.module("cratis-shop-checkout", ["cratis-shop-cart", 'ng.django.urls']);

app.controller('CheckoutCtrl', ['$scope', '$rootScope', 'Cart', 'djangoRMI', function ($scope, $rootScope, Cart, djangoRMI) {

    $scope.cart = Cart;

    $scope.settings = Cart.checkout;
    $scope.invoiceData = {}
    $scope.countries = CONST_COUNTRIES;


    $rootScope.$on('cart.update', function() {
        if ($scope.cart.cart.total == 0) {
            location.href = '/';
        }
    });

    $scope.saveSettings = function() {
//        if ($scope.settings.address && $scope.settings.address.country) {
//            Cart.country = $scope.settings.address.country;
//            Cart.updateCart();
//        }
        Cart.SaveState();
    };

    $scope.invoiceCheckout = function() {
            Cart.clear();
    }

    var doCheckout = function(addressID, payment_url) {

        var order = angular.copy(Cart.cart);
        order.currency = Cart.currency;

        order.address = addressID;

        djangoRMI.shop_checkout.checkout(order).success(function(data) {


            $.ambiance({message: "Спаисбо за покупку!",
			                    title: "Оформление заказа",
			                    type: "success"});

            Cart.clear();

            if (payment_url) {
                location.href = payment_url;
            } else {
                location.href = '/accounts/profile/';
            }
        });
    };

    $scope.checkout = function(payment_url) {

        if (!$scope.settings.address || $scope.settings.address.addressID == 0) {
            var data = angular.copy($scope.form.customAddress);
            data.country = data.country.code;

            djangoRMI.cratis_profile__main.save_address(data).success(function(data) {

                var addressID = data['addressID'];
                doCheckout(addressID, payment_url);
            });
        } else {
            doCheckout($scope.settings.address, payment_url);
        }
    };

    if (djangoRMI.cratis_profile__main) {
        djangoRMI.cratis_profile__main.get_addresses().success(function(data) {
            $scope.addresses = [];
            $scope.addresses.push({asddressID: 0, title: 'Новый аддрес'});
            angular.forEach(data, function(address) {
                if (address.country) {
                    address.country = CONST_COUNTRIES[address.country];
                } else {
                    address.country = None
                }

                $scope.addresses.push(address);
            })
        });
    }
}]);
