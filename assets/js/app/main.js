

var app = angular.module('grandexApp', [
    'ngRoute', 'ng.django.urls', 'ui.slider', 'ng-breadcrumbs', 'ng.django.forms', 'angularSpinner', 'gettext',
    'ng.django.rmi'
]);

app.config(['$interpolateProvider', function ($interpolateProvider) {
        $interpolateProvider.startSymbol('{[').endSymbol(']}');

//        gettextCatalog.setCurrentLanguage(LANG_CODE);
    }]
);

app.run(['gettextCatalog', function (gettextCatalog) {
    gettextCatalog.setCurrentLanguage(LANG_CODE);
}]);

app.filter('safe', ['$sce', function($sce){
        return function(text) {
            return $sce.trustAsHtml(text);
        };
}]);
//
//app.config(function ($interpolateProvider, $routeProvider) {
//        $interpolateProvider.startSymbol('{[').endSymbol(']}');
//        $routeProvider.
//            when('/_', {
//                templateUrl: '/static/js/app/template/main.html',
//                controller: 'MainCtrl'
//            }).
////            when('/order', {
////                templateUrl: '/static/js/room_service/template/order.html',
////                controller: 'OrderCtrl'
////            }).
//            otherwise({
//                redirectTo: '/_'
//            });
//
//    }
//);
