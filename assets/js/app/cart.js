var app = angular.module("cratis-shop-cart", ['ng.django.rmi', 'ng.django.urls']);

//app.directive('room', function() {
//  return {
//    require: '?ngModel',
//    link: function(scope, ele, attrs, ngModel) {
//
//        ngModel.$parsers.unshift(function (viewValue) {
//
//            var valid = false;
//            if (201 <= viewValue && viewValue <= 215) {
//                valid = true;
//            } else if (301 <= viewValue && viewValue <= 315) {
//                valid = true;
//            } else if (401 <= viewValue && viewValue <= 415) {
//                valid = true;
//            } else if (501 <= viewValue && viewValue <= 515) {
//                valid = true;
//            } else if (601 <= viewValue && viewValue <= 615) {
//                valid = true;
//            } else if (702 <= viewValue && viewValue <= 709) {
//                valid = true;
//            }
//
//            ngModel.$setValidity('roomNumber', valid);
//            return viewValue;
//        });
//    }
//  }
//});

app.filter('checkmark', function() {
  return function(input) {
    return input ? '\u2713' : '\u2718';
  };
});


app.filter('cart_price', ['Cart', '$filter', function(Cart, $filter) {
  return function(price, vat) {

      vat = vat || false;
      price = parseFloat(price);
      if (vat && Cart.tax) {
          price = price + price * Cart.tax_rate / 100;
      }
      if (Cart.currency) {
        return $filter('number')(price / Cart.currency.rate, 2) + ' ' + Cart.currency.symbol;
      } else {
        return $filter('number')(price, 2);
      }
  };
}]);

app.service('Cart', ['$rootScope', '$http', 'djangoRMI', function ($rootScope, $http, djangoRMI) {

    var storagePrefix = 'v5-';


    var service = {

        defaultCountry: null,

        currencies: currencies,
        tax_rate: tax_rate,

        currency: null,
        user: 0,

        checkout: {}, // checkout settings

        tax: true,

        cart: {
            items: {},
            total: 0,
            totalItems: 0,
            totalQty: 0
        },

        inCart: function (product) {
            return !!service.cart.items[product.id];
        },

        setCurrency: function (currency) {
            service.currency = currency;
            service.updateCart();
        },

        toggleTax: function () {
            service.tax = !service.tax;
            service.updateCart();
        },

        getItem: function (product) {
            if (!service.inCart(product)) {
                return null;
            }
            return service.cart.items[product.id];
        },

        variationsInCart: function (product) {
            var inCart = false;

            angular.forEach(product.variations, function (id) {
                if (id != product.id && service.cart.items[id]) {
                    inCart = true;
                }
            });

            return inCart;
        },

        hasShopProducts: function () {
            var inCart = [];

            angular.forEach(service.cart.items, function (item, key) {
                if (item.meta.type == 'shop_product') {
                    inCart.push(item);
                }
            });

            return inCart.length > 0;
        },

        cartQty: function (product) {
            if (!service.inCart(product)) {
                return 0;
            }
            return service.cart.items[product.id].qty;
        },

        add: function (product, qty) {

            if (!qty) {
                qty = 1;
            }

            if (!service.inCart(product)) {
                var copy = angular.copy(product);
                copy.qty = qty;
                service.cart.items[product.id] = copy;
            } else {
                service.cart.items[product.id].qty = parseInt(service.cart.items[product.id].qty) + parseInt(qty);
            }


            $rootScope.$broadcast('cart.add');

            service.updateCart();
        },

        remove: function (product) {
            if (service.inCart(product)) {
                service.cart.items[product.id].qty -= 1;
                if (service.cart.items[product.id].qty < 1) {
                    delete service.cart.items[product.id];
                }
            }

            service.updateCart();
        },

        removeAll: function (product) {
            if (service.inCart(product)) {
                delete service.cart.items[product.id];
            }

            service.updateCart();
        },

        merge: function(newData) {
            angular.forEach(newData.items, function (item, key) {
                if (item.qty > 0) {
                    service.add(item, item.qty);
                }
            });
        },

        clear: function () {
            service.cart = {
                items: {},
                totalItems: 0,
                total: 0,
                totalQty: 0,
                totalDelivery: 0
            };

            service.updateCart();
        },

        updateCart: function () {
            var total = 0;
            var totalQty = 0;
            var totalItems = 0;
            var totalDelivery = 0;

            angular.forEach(service.cart.items, function (item, key) {
                if (item.qty == 0) {
                    delete service.cart.items[key];
                } else {
                    total += item.price * item.qty;
                    totalQty += item.qty;
                    totalItems += 1;
                }
                if (service.country) {
                    var rowDelivery = service.country.calculateRowDelivery(item);
                    item.deliveryPrice = rowDelivery.result;
                    item.deliveryDescription = rowDelivery.description;
                    totalDelivery += item.deliveryPrice;
                }
            });

            service.cart.total = total;
            service.cart.totalQty = totalQty;
            service.cart.totalItems = totalItems;

//            if (service.country) {
//                var totalDeliveryCalc = service.country.calculateTotalDelivery({
//                    total: total,
//                    totalQty: totalQty,
//                    totalItems: totalItems,
//                    totalDelivery: totalDelivery
//                });
//                service.cart.totalDelivery = totalDeliveryCalc.result;
//                service.cart.totalDeliveryDescription = totalDeliveryCalc.description;
//                service.cart.totalWithDelivery = total + totalDeliveryCalc.result;
//            } else {
                service.cart.totalDelivery = 0;
                service.cart.totalDeliveryDescription = '';
                service.cart.totalWithDelivery = total + 0;
//            }

            if (service.cart.totalItems > 0) {

            }
            service.SaveState();

            if (service.cart.totalItems > 0) {
                $rootScope.$broadcast('cart.update');
            } else {
                $rootScope.$broadcast('cart.clear');
            }
        },

        submitCart: function() {
            alert('Submitted');
//            return $http.post('/room_service_order', {'order': service.cart, type: data.type});
        },

        SaveState: function () {

            if (service.currency) {
                localStorage[storagePrefix + 'currency'] = service.currency.code;
            } else {
                localStorage[storagePrefix + 'currency'] = '$';
            }
            if (service.country) {
                localStorage[storagePrefix + 'country'] = service.country.code;
            } else {
                localStorage[storagePrefix + 'country'] = 'ee';
            }

            localStorage[storagePrefix + 'cart'] = angular.toJson(service.cart);
            localStorage[storagePrefix + 'tax'] = angular.toJson(service.tax);
            localStorage[storagePrefix + 'checkout'] = angular.toJson(service.checkout);

            localStorage[storagePrefix + 'user'] = service.user;

            if (service.user) {
                console.log('Save', service.cart);
                djangoRMI.shop_checkout.save_cart(service.cart).success(function(data) {});
            }
        },

        RestoreState: function () {
            if (angular.fromJson(localStorage[storagePrefix + 'tax']) === false) {
                service.tax = false;
            }
            if (localStorage[storagePrefix + 'currency']) {
                service.currency = service.currencies[localStorage[storagePrefix + 'currency']];
            } else {
                service.currency = service.currencies.DEFAULT;
            }
            if (localStorage[storagePrefix + 'country']) {
                service.country = CONST_COUNTRIES[localStorage[storagePrefix + 'country']];
            } else {
                service.country = service.defaultCountry;
            }

            if (localStorage[storagePrefix + 'checkout']) {
                service.checkout = angular.fromJson(localStorage[storagePrefix + 'checkout']);
            } else {
                service.checkout = {};
            }

            if (localStorage[storagePrefix + 'user']) {
                service.user = angular.fromJson(localStorage[storagePrefix + 'user']);
            } else {
                service.user = 0;
            }

            if (localStorage[storagePrefix + 'cart']) {
                service.cart = angular.fromJson(localStorage[storagePrefix + 'cart']);
            }

            if (USER_ID > 0) {

                djangoRMI.shop_checkout.load_cart().success(function(data) {

                    if (data.exists) {
                        if (USER_ID != service.user) {
                            service.user = USER_ID;
                            service.tax = !USER_IS_COMPANY;

                            if (data.order) {
                                service.merge(data.order);
                            }

                            service.updateCart();
                        }

                    } else {

                        service.user = USER_ID;
                        service.tax = !USER_IS_COMPANY;

                        if (service.cart.totalItems > 0) {
                            service.clear();
                        }
                    }


                });
            } else if (USER_ID == 0 && 0 != service.user) {
                service.user = 0;
                service.clear();
            }


        }
    };

    angular.forEach(CONST_COUNTRIES, function(country) {

        if (!service.defaultCountry) {
            service.defaultCountry = country;
        }

        country.weightIndex = function(weight) {
            if (!weight) {
                return 0;
            }

            var val = 0;
            angular.forEach(country.weight_ranges, function(r) {
                if (weight >= r.from && weight < r.to) {
                    console.log(r.from, r.to, weight);
                    val = r.value;
                }
            });

            return val;
        };

        if (country.row_delivery) {
            country.calculateRowDelivery = function(product) {
                var result = 0;
                var description = '';

                var data = {};
                angular.forEach(product, function(value, key) {
                    data['product__' + key] = value;
                });
                data['weight_index'] = country.weightIndex(product.netWeight || 0);
                console.log('Product ' + product.name + ' row vars', data);
                eval(country.row_delivery);

                return {result: result, description: description};
            };
        } else {
            country.calculateRowDelivery = function() {
                return 0;
            }
        }

        if (country.total_delivery) {
            country.calculateTotalDelivery = function(data) {
                var result = 0;
                var description = '';

                data.express_delivery_index = country.express_delivery_index;
                data.free_delivery_at = country.free_delivery_at;
                eval(country.total_delivery);

                return {result: result, description: description};
            };
        } else {
            country.calculateTotalDelivery = function() {
                return 0;
            }
        }

    });

    service.RestoreState();

    return service;
}]);



app.controller('MainCtrl', ['$scope', '$routeParams', '$http', 'Cart', function ($scope, $routeParams, $http, Cart) {
    $scope.cart = Cart;
    $scope.search = {
        q: ''
    };

    $scope.countries = CONST_COUNTRIES;


}]);

app.controller('HeaderCtrl', ['$scope', '$routeParams', '$http', 'Cart', function ($scope, $routeParams, $http, Cart) {}]);

//
//function followCartUpdates($scope, Cart) {
//    $scope.$on('cart.update', function (event) {
//        $scope.cart = Cart.cart;
//    });
//    $scope.cart = Cart.cart;
//}
//
//app.controller('SearchCtrl', function ($scope, $rootScope, $location, Cart) {
//
//    $scope.cart = Cart;
////    followCartUpdates($scope, Cart);
//});
