var app = angular.module("cratis-shop-product", ['ng.django.rmi', 'ng.django.urls']);


app.config(['$interpolateProvider', '$routeProvider', function ($interpolateProvider, $routeProvider) {

        $routeProvider.
            when('/:variation', {
                templateUrl: '/static/js/app/templates/product.html',
                controller: 'ProductCtrl'
            }).
//            when('/order', {
//                templateUrl: '/static/js/room_service/template/order.html',
//                controller: 'OrderCtrl'
//            }).
            otherwise({
                redirectTo: '/' + product_data.variation_default
            });

    }]
);

app.directive('productSlider', function () {
    return {
        restrict: 'E',
        templateUrl: '/static/js/app/templates/product_slider.html',

        scope: {
            pics: '=images'
        },

        link: function (scope, elm, attrs) {
            elm.hide();
            elm.ready(function() {
                setTimeout(function(){
                    elm.show();
                  elm.find('.product_slider ul').bxSlider({
                        pagerCustom: '.slider_wrapper .product_pager'
                  });

                }, 100);
            });
        }
    };
});

app.directive('productAdd', ['Cart', function (Cart) {
    return {
        restrict: 'E',
        templateUrl: '/static/js/app/templates/product_add_el.html',

        scope: {
            product: '=product'
        },

        link: function (scope, elm, attrs) {

            scope.qty = 1;

            scope.incr = function() {
                scope.qty += 1;
            };

            scope.decr = function() {
                scope.qty -= 1;
            };

            scope.addToCart = function() {
                Cart.add(scope.product, scope.qty);
                scope.qty = 1;

                $.ambiance({message: "Товар добавился в корзинку.",
			                    title: "Корзинка",
			                    type: "success"});
            }
        }
    };
}]);


app.controller('ProductCtrl', ['$scope', '$routeParams', '$http', function ($scope, $routeParams, $http) {
//    $scope.trans = trans;

//    followCartUpdates($scope, Cart);

    if (!product_data.has_variations) {
        $scope.product = product_data.product;
        return;
    } else {
        $scope.parent_product = product_data.product;
    }

    $scope.selection = {};


//    $scope.product = product_data.product;
    $scope.variation_fields = product_data.variation_fields;
    $scope.variation_products = product_data.variation_products;

    var product_id = product_data.variation_products[$routeParams['variation']];

    $http.get(lang_prefix + 'product_data/' + product_id).success(function(data) {
        $scope.product = data;
    });

    var selected_variations = $routeParams['variation'].split('-');


    angular.forEach(product_data.variation_fields, function(field, id) {
        angular.forEach(field.values, function(value, code) {
            if (selected_variations.indexOf(code) >= 0) {
                $scope.selection[id] = code;
            }
        });
    });

    $scope.selection_changed = function() {
        var selections = [];
        angular.forEach($scope.selection, function(code) {
            selections.push(code);
        });

        location.hash = '#/' + selections.sort().join('-');
    };
}]);
