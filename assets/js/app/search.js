
var app = angular.module("cratis-shop-search", ["cratis-shop-cart", 'ng.django.urls']);


app.controller('SearchCtrl', ['$scope', '$http', 'Cart', 'djangoUrl', '$location', function ($scope, $http, Cart, djangoUrl, $location) {

//    $scope.search = {};

    $scope.category = $location.search().category;


    $scope.cart = Cart;

    $scope.conf = loadConf() || {
        sorting: 'lowest',
        page_size: 10,
        page_type: 'col',
        page: 1

    };

    $scope.slider = {
        range: [0, 1000],
        limits: {
            min: 0,
            max: 1000
        }
    };

    var facet_selections = {};

    $scope.setPageSize = function(size) {
        console.log(size);
        $scope.conf.page_size = size;
        $scope.conf.page = 1;
        doSearch();
    };

    $scope.setPage = function(page) {
        $scope.conf.page = page;
        doSearch();
    };

    $scope.doSort = function() {
        $scope.conf.page = 1;
        doSearch();
    };

    $scope.selectFacet = function(name, entry) {

        value = entry.term;

        angular.forEach($scope.result.products.facets[name].terms, function(term) {

            if (term.term == value) {
                // already selected
                if (term.selected) {
                    term.selected = false;
                    delete facet_selections[name];
                } else {
                    term.selected = true;
                    facet_selections[name] = value;
                }
            } else {
                term.selected = false;
            }
        });

        $scope.conf.page = 1;

        doSearch();
    };
    $scope.setPageType = function(type) {
        $scope.conf.page_type = type;
        saveConf();
    };

//    if (data.query) {
//        $scope.search.q = data.query;
//    }

    $scope.searching = false;

    function doSearch() {
        if ($scope.searching) {
            return;
        }
        $scope.searching = true;

        var params = angular.copy(facet_selections);
        if ($location.search().category) {
            params['category'] = $location.search().category;
        }

        console.log(params);

//        if ($scope.search.q) {
//            params['q'] = $scope.search.q;
//        }

        params['sort'] = $scope.conf.sorting;
        params['size'] = $scope.conf.page_size;
        params['page'] = $scope.conf.page;
        if ($scope.slider.range[0] != 0) {
            params['priceMin'] = $scope.slider.range[0];
        }
        if ($scope.slider.range[1] != 0) {
            params['priceMax'] = $scope.slider.range[1];
        }

        $http.get(djangoUrl.reverse('search_data'), {params: params}).success(function(data) {

            if (data.category) {
                angular.forEach(data.category.subGroups, function(cat) {
                    cat.url = djangoUrl.reverse('shop_category', {'id': cat.productGroupID})
                });
            }

            console.log(data);

            $scope.result = data;

            if (data.min == data.max) {
                data.min = 0;
            }

//            $scope.slider.limits.min = data.min;
//            $scope.slider.limits.max = data.max;

//            if (!$scope.slider.range) {
//                $scope.slider.range = [data.min, data.max]
//            }

            $scope.searching = false;
        });

        saveConf();
    }

    function saveConf() {
        localStorage['v2-search_conf'] = angular.toJson($scope.conf);
    }
    function loadConf() {
        if (localStorage['v2-search_conf']) {
            var val = angular.fromJson(localStorage['v2-search_conf']);
            val['page'] = 1;
            val['priceMin'] = 0;
            val['priceMax'] = 0;
            return  val;
        }
    }
    doSearch();

    $scope.$watch('search.q', doSearch);
    $scope.$watch('conf.sorting', $scope.doSort);
    $scope.$watch('slider.range', doSearch);
    $scope.$watch('category', doSearch);


}]);
