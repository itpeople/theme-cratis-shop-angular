$(function () {
    var textarea = $('.cratis-rule-field');

    if (textarea) {
        textarea.hide();


        Blockly.inject(document.getElementById('blocklyDiv'),
            {path: '/static/blockly/', toolbox: document.getElementById('toolbox')});

        function myUpdateFunction() {

            var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
            var xml_text = Blockly.Xml.domToText(xml);

            var jsCode = Blockly.JavaScript.workspaceToCode();
            var pythonCode = Blockly.Python.workspaceToCode();

            $('#blocklyCode').html(jsCode);
            $('#blocklyCodePython').html(pythonCode);

            var json = JSON.stringify({xml: xml_text, js: jsCode, python: pythonCode});
            $('.cratis-rule-field').val(json);
        }

        try {
            var data = JSON.parse($('.cratis-rule-field').val());
            if (data.xml) {
                var xml = Blockly.Xml.textToDom(data.xml);
                Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
            }
        } catch (e) {
        }

        Blockly.addChangeListener(myUpdateFunction);
    }
});