
//
//
//var range = new Array();
//range.min = 20; // ÐœÐ¸Ð½Ð¸Ð¼Ð°Ð»ÑŒÐ½Ð°Ñ Ð³Ñ€Ð°Ð½Ð¸Ñ†Ð°
//range.max = 300; // ÐœÐ°ÐºÑÐ¸Ð¼Ð°Ð»ÑŒÐ½Ð°Ñ Ð³Ñ€Ð°Ð½Ð¸Ñ†Ð°
//range.min_value = 40; // Ð—Ð½Ð°Ñ‡ÐµÐ½Ð¸Ðµ Ð¼Ð¸Ð½Ð¸Ð¼Ð°Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ñ„Ð»Ð°Ð¶ÐºÐ°
//range.max_value = 250; // Ð—Ð½Ð°Ñ‡ÐµÐ½Ð¸Ðµ Ð¼Ð°ÐºÑÐ¸Ð¼Ð°Ð»ÑŒÐ½Ð¾Ð³Ð¾ Ñ„Ð»Ð°Ð¶ÐºÐ°
//

$(document).ready(function(){

    $('.home_slider ul').bxSlider();

    // Двигаем попап по изменению ширины окна
    $(window).resize(function(){
        change_width_popup('cart_popup');
    });

    $('.small_nav select, .sort_block .select select').styler();

    $('#fade').click(function(){
        close_popup();
    });

//    $('.small_nav select, .select select, .styler, #id_country').styler();
    
//    $('.close').click(function(){
//        $(this).parent().remove();
//    });

    $('.slide_nav').click(function(){

        var li = $(this).parent('li');
        var ul = $(this).siblings('ul');
        if($(li).hasClass('active'))
        {
            $(ul).slideUp();
            $(li).removeClass('active');
        }
        else
        {
            $(ul).slideDown();
            $(li).addClass('active');
        }        

    })
//
//    $( ".curr li a" ).click(function() {
//        $.cookie("curr", $(this).parent().data("curr"), { expires: 7, path: '/' });
//        location.reload();
//    });
//
//    if ($.cookie("curr") != undefined) {
//      $(".curr li").removeClass("active");
//      $(".curr li[data-curr='" + $.cookie("curr") + "']").addClass("active");
//    }
//
//    if($('div').hasClass('filter_price'))
//    {
//        price_range_ini();
//    }

    $('.message .close').click(function(){
        $(this).parent().fadeOut();
    })

    $('.orders_info_list .more a').click(function(){
        $(this).parents('li').addClass('active');
    });

    $('.orders_info_list .close').click(function(){
        $(this).parents('li').removeClass('active');
    });

    $('#pay_type li input[type="radio"]:checked').parent().addClass('active');

    $('#pay_type li').click(function(){
        $('#pay_type li').removeClass('active');
        $(this).addClass('active').find('input[type="radio"]').prop('checked', 'checked');
    });
    
})

function open_lang()
{
    var parent = $('header .lang_wrap .lang');
    if($(parent).hasClass('active'))
    {
        $(parent).removeClass('active');
    }
    else
    {
        $(parent).addClass('active');
    }
}

function open_popup(id_popup)
{
    if ($("#" + id_popup).css("display") == "block") {
        $("#" + id_popup).hide();
        $("#fade").hide();
        $("body").css({"overflow": "visible"});
    }
    else {
        $(".popup").hide();
        var scroll_top = $(document).scrollTop();
        change_width_popup(id_popup);
        //Ширина и высота окна браузера
        $("#fade").fadeTo("slow", 1);
        //Расположение модального окна с содержимым по высоте учитывая скроллинг документа
        $("#" + id_popup).css({"display": "block"});
        //Запрет на сколлинг страницы
        $(document).scrollTop(scroll_top + "px");
        $("body").css({"overflow": ":visible"});
    }
}

function change_width_popup(id_popup)
{
    var HeightDocument = $(document).height();
    var WidthDocument = $(document).width();
    var HeightWin = $(window).height();
    var WidthWin = $(window).width();
    var HeightScreen = $(window).height();
    var pop_top = HeightScreen / 2 - $("#" + id_popup).height() / 2 + "px";
    var pop_left = WidthWin / 2 - $("#" + id_popup).width() / 2 + "px";
    $("#" + id_popup).css({"top": pop_top, "left": pop_left});
}

function close_popup()
{
    $(".popup").hide();
    $("#fade").hide();
}

//
//function price_range_ini()
//{
//    $(".slider-range").slider({
//        range: true,
//        min: range.min,
//        max: range.max,
//        values: [range.min_value, range.max_value],
//        slide: function( event, ui ) {
//            $('.min_price').val(ui.values[0]);
//            $('.max_price').val(ui.values[1]);
//            set_price_range();
//        }
//    });
//    $('.min_price').val(range.min_value);
//    $('.max_price').val(range.max_value);
//
//}
//
//function set_price_range()
//{
//    var min = parseInt($('.min_price').val());
//    var max = parseInt($('.max_price').val());
//    if(min<range.min||min>range.max||min>max||!min){min = range.min;}
//    if(max>range.max||max<range.min||max<min||!max){max = range.max;}
//    $(".slider-range").slider("values", [min, max]);
//    $('.min_price').val(min);
//    $('.max_price').val(max);
//}

function open_absolute_filters()
{
    if($('.absolute_filters').hasClass('active'))
    {
        $('.absolute_filters').fadeOut().removeClass('active');
    }
    else
    {
        $('.absolute_filters').fadeIn().addClass('active');
    }

}