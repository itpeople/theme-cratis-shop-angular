function BasketController($scope){

	function init() {
		if ($.cookie('cart') == undefined) {
		    var dt = new Date();
			var id = dt.getSeconds() + (60 * dt.getMinutes()) + (60 * 60 * dt.getHours());
			$.cookie('cart', id, { path: '/' })
		}		
	}
	init();

	$scope.removeBasket = function(product_id, obj) {
		url = $(obj.target).attr('data');
		cart = $.cookie('cart');
		$.get(url, {remove_id : product_id, cart: cart}  ,function( data ) {
			obj.target.parentNode.parentNode.remove();
			$('#header-basker-count').html(data.count);
			$('#header-basker-price').html(data.price);
			$('#basket-total-price').html(data.price);
		});
	}


	$scope.minusBasket = function(product_id, count, obj) {
		url = $(obj.target).attr('data');
		cart = $.cookie('cart');
		if ($(obj.target).next().val() > 0) {
			$(obj.target).next().val(parseInt($(obj.target).next().val()) - 1);
		}

		update_count = $(obj.target).next().val();
		update_id = product_id;
		$.get(url, {update_id : update_id, update_count: update_count, cart: cart}  ,function( data ) {
			$('#header-basker-count').html(data.count);
			$('#header-basker-price').html(data.price);
			$('#basket-total-price').html(data.price);
		});
	}

	$scope.plusBasket = function(product_id, count, obj) {
		url = $(obj.target).attr('data');
		cart = $.cookie('cart');
		$(obj.target).prev().val(parseInt($(obj.target).prev().val()) + 1);

		update_count = $(obj.target).prev().val();
		update_id = product_id;
		$.get(url, {update_id : update_id, update_count: update_count, cart: cart}  ,function( data ) {
			$('#header-basker-count').html(data.count);
			$('#header-basker-price').html(data.price);
			$('#basket-total-price').html(data.price);
		});
	}

	$scope.addBasket = function(product_id, price, amount) {
			if (amount = undefined) {
				amount = 1;
			}
			cart = $.cookie('cart');
			url = $('.add-basket').attr('data-url');
            $.ambiance({message: "We added product in basket",
			                    title: "Success!",
			                    type: "success"});
			$.get(url, { product_id: product_id, 
						 price: price, 
						 amount: amount, cart: cart},  function( data ) {
						 location.reload();
			});
	}
}